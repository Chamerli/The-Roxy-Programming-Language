
use crate::token::{TokenKind, Token};

#[derive(Debug)]
pub struct Lexer {
    source: Vec<char>,
    index: usize,
}

impl Lexer {
    // Here's the missing 'new' function to create a Lexer instance
    pub fn new(content: String) -> Self {
        Self {
            source: content.chars().collect(),
            index: 0,
        }
    }

    pub fn lex(&mut self) -> Vec<Token> {
        let mut tokens: Vec<Token> = Vec::new();

        while self.source.len() > self.index {
            let c = self.current_char();

            match c {
                '=' => {
                    tokens.push(Token::new(TokenKind::Assign, "=".to_owned()));
                    self.index += 1
                },
                '+' => {
                    tokens.push(Token::new(TokenKind::Addition, "+".to_owned()));
                    self.index += 1
                }, 
                '-' => {
                    tokens.push(Token::new(TokenKind::Subtraction, "-".to_owned()));
                    self.index += 1
                },
                '*' => {
                    tokens.push(Token::new(TokenKind::Multiplication, "*".to_owned()));
                    self.index += 1
                },
                '/' => {
                    tokens.push(Token::new(TokenKind::Division, "/".to_owned()));
                    self.index += 1
                },
                '"' | '\'' => {
                    self.index += 1;
                    
                    let mut buffer: String = String::new();

                    while self.current_char() != c {
                        buffer.push(self.current_char());
                        self.index += 1;
                    }

                    tokens.push(Token::new(TokenKind::String, buffer));
                    self.index += 1;
                },
                _ if c.is_numeric() => {
                    let mut buffer: String = String::new();
                    buffer.push(c);

                    let mut kind: TokenKind = TokenKind::Number; // Number or Float
                    self.index += 1;

                    while self.current_char().is_numeric() || self.current_char() == '.' {
                        buffer.push(self.current_char());
                        if self.current_char() == '.' {
                            kind = TokenKind::Float;
                        }
                        self.index += 1;
                    }

                    tokens.push(Token::new(kind, buffer))
                },
                _ if c.is_alphabetic() => {
                    let mut buffer: String = String::new();
                    buffer.push(c);

                    self.index += 1;
                    while self.current_char().is_alphabetic() {
                        buffer.push(self.current_char());
                        self.index += 1;
                    }

                    let kind: TokenKind = match buffer.as_str() {
                        "var" => TokenKind::Var,
                        _ => TokenKind::Identifier,
                    };

                    tokens.push(Token::new(kind, buffer))
                }
                _ => {
                    self.index += 1
                },
            }
        }
        tokens // Return the tokens instead of printing them
    }

    fn current_char(&self) -> char {
        *self.source.get(self.index).unwrap()
    }
}

