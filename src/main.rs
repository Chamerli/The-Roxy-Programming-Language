
use std::{fs, env};

pub mod token;
pub mod lexer;
pub mod parser;

fn main() {
    let file: String = match env::args().nth(1) {
        Some(file) => file,
        None => {
            eprintln!("error: no file provided");
            std::process::exit(1);
        }
    };

    let content: String = match fs::read_to_string(file) {
        Ok(content) => content,
        Err(err) => {
            eprintln!("error reading file: {}", err);
            std::process::exit(1);
        }
    };

    // Create a new lexer instance using Lexer::new, not lexer::new
    let mut lexer = lexer::Lexer::new(content);
    let tokens = lexer.lex(); // Get the tokens from the lexer

    // Initialize the parser with the tokens
    let mut parser = parser::Parser::new(&tokens);
    
    // Parse the program
    match parser.parse_program() {
        Ok(statements) => {
            // Print parsed statements
            println!("{:?} \n", statements);
        }
        Err(err) => {
            eprintln!("Parsing error: {}", err);
        }
    }
}

