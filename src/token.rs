#[derive(Debug)]
pub enum TokenKind {

    // Keywords
    Var, // 'var'


    // Operations:
    Identifier, // THE VAR NAME
    Assign, // '='
    Addition, // '+'
    Subtraction, // '-'
    Multiplication, // '*'
    Division, // '/'

    // Var types:
    String,
    Number,
    Float,
    Bool

}

#[derive(Debug)]
pub struct Token {
    pub kind: TokenKind, 
    pub literal: String,
}
impl Token {
    pub fn new(kind: TokenKind, literal: String) -> Self {
        Self {
            kind,
            literal
        }
    }
}
