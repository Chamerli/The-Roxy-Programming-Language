pub mod ast_node;


#[derive(Debug)]
pub enum ASTNode {
    // Represents a variable declaration, including the variable's name and the value assigned to it.
    VarDecl {
        name: String,        // The name of the variable being declared (e.g., "name")
        value: Box<ASTNode>, // The value assigned to the variable (could be a literal or another expression)
    },

    // Represents a variable reference or identifier (e.g., "x" in `x + 1`)
    Identifier(String),      // The name of the variable being referenced

    // Represents a literal number (integer or float)
    Literal(f64),           // A numeric literal, such as `3.14` or `42`

    // Represents a string literal (e.g., "Hello, world!")
    StringLiteral(String),  // A string literal, such as `"Hello, world!"`

    // Represents a binary operation (e.g., `x + y` or `a * b`)
    BinaryOp {
        left: Box<ASTNode>,  // Left operand of the binary operation
        operator: TokenKind, // The operator (e.g., `+`, `-`, `*`, `/`)
        right: Box<ASTNode>, // Right operand of the binary operation
    },

    // Represents an assignment operation (e.g., `x = 5`)
    Assign {
        target: Box<ASTNode>, // The variable being assigned to (the left-hand side, e.g., `x`)
        value: Box<ASTNode>,  // The value being assigned (the right-hand side, e.g., `5`)
    },

    // Additional node types like function calls, conditionals, etc., can be added here as needed
}

impl ASTNode {
    // Creates a new variable declaration node.
    // Used when a variable is being declared and initialized (e.g., `var name = "Alice";`)
    pub fn new_var_decl(name: String, value: ASTNode) -> Self {
        ASTNode::VarDecl {
            name,
            value: Box::new(value),
        }
    }

    // Creates a new identifier node.
    // Used when referring to an existing variable (e.g., `name` in `name + 5`)
    pub fn new_identifier(name: String) -> Self {
        ASTNode::Identifier(name)
    }

    // Creates a new numeric literal node.
    // Used for both integers and floats (e.g., `42`, `3.14`)
    pub fn new_literal(value: f64) -> Self {
        ASTNode::Literal(value)
    }

    // Creates a new string literal node.
    // Used for string values (e.g., `"Hello, world!"`)
    pub fn new_string_literal(value: String) -> Self {
        ASTNode::StringLiteral(value)
    }

    // Creates a new binary operation node.
    // Used for operations that involve two operands (e.g., `x + y`, `a * b`)
    pub fn new_binary_op(left: ASTNode, operator: TokenKind, right: ASTNode) -> Self {
        ASTNode::BinaryOp {
            left: Box::new(left),
            operator,
            right: Box::new(right),
        }
    }

    // Creates a new assignment node.
    // Used for variable assignments (e.g., `x = 5`)
    pub fn new_assign(target: ASTNode, value: ASTNode) -> Self {
        ASTNode::Assign {
            target: Box::new(target),
            value: Box::new(value),
        }
    }
} // SHIT WE HAVE MORE COMMENTS THAN ACTUAL CODE IN THIS BITCH!
